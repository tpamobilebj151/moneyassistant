package model;

/**
 * Created by Kevin Nathanael on 12/30/2015.
 */
public class MyMoney {
    private static float expense;
    private static float income;

    public static boolean checkNegative(float v) {
        if(v < 0)
            return true;
        return false;
    }
    public static void setZero() {
        expense = income = 0;
    }

    public static float getExpense() {
        return expense;
    }

    public static void setExpense(float expense) {
        MyMoney.expense = expense;
    }

    public static float getIncome() {
        return income;
    }

    public static void setIncome(float income) {
        MyMoney.income = income;
    }
}
