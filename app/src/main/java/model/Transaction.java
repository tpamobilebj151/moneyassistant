package model;

import java.util.Date;

/**
 * Created by Kevin Nathanael on 12/30/2015.
 */
public class Transaction {

    private String uid;
    private float money;
    private String category;
    private Date date;
    private String desc;

    public Transaction() {    }

    public Transaction(String uid, float money, String category, Date date, String desc) {
        this.uid = uid;
        this.money = money;
        this.category = category;
        this.date = date;
        this.desc = desc;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public float getMoney() {
        return money;
    }

    public void setMoney(float money) {
        this.money = money;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
