package helper;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.util.Log;
import android.widget.Button;
import android.widget.Toast;

import com.facebook.login.LoginManager;
import com.firebase.client.AuthData;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.Query;
import com.firebase.client.ValueEventListener;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import model.MyMoney;
import model.Transaction;

import edu.bluejack151.moneyassitant.LoginActivity;
import edu.bluejack151.moneyassitant.MainActivity;

import model.User;

/**
 * Created by Kevin Nathanael on 12/30/2015.
 */
public class Connection {

    private Firebase ref;
    private Context context;
    private SharedPreferences sp;

    public Connection(Context context, String url) {
        this.context = context;
        sp = context.getSharedPreferences(StrHelper.SP_USER, Context.MODE_PRIVATE);
        Firebase.setAndroidContext(this.context);
        ref = new Firebase(url);
    }

    /* LOGIN */
    public void checkAuth() {
        // jika user telah log in
        if(sp.getBoolean(StrHelper.SP_CHECK_LOGIN, false)) {
            moveActivity();
        }
    }

    public void login(final String email, String password, final ProgressDialog progressDialog, final Button loginButton) {
        android.util.Log.v("yes", "masuk login");
        ref.authWithPassword(email, password, new Firebase.AuthResultHandler() {
            @Override
            public void onAuthenticated(AuthData authData) {
                android.util.Log.v("yes", "login oke");
                setLoginData(false, authData.getUid(), email, authData.getToken());
                progressDialog.dismiss();
                moveActivity();
            }

            @Override
            public void onAuthenticationError(FirebaseError firebaseError) {
                loginButton.setEnabled(true);
                progressDialog.dismiss();
                showFirebaseError(firebaseError);
            }
        });
    }

    public void loginViaFacebook(final String email, final String token, final String idFacebook) {
        ref.authWithOAuthToken("facebook", token, new Firebase.AuthResultHandler() {
            @Override
            public void onAuthenticated(AuthData authData) {
                setLoginData(true, authData.getUid(), email, token);
                sp.edit().putString(StrHelper.SP_FB_ID, idFacebook).apply();
                moveActivity();
            }

            @Override
            public void onAuthenticationError(FirebaseError firebaseError) {
                showFirebaseError(firebaseError);
            }
        });
    }

    private void moveActivity() {
        Intent in = new Intent(context, MainActivity.class);
        in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(in);
        ((Activity)context).finish();
    }

    public void setLoginData(boolean facebook, String uid, String email, String token) {
        sp.edit().putBoolean(StrHelper.SP_CHECK_LOGIN, true).apply();
        sp.edit().putBoolean(StrHelper.SP_FB_CHECK, facebook).apply();
        sp.edit().putString(StrHelper.SP_USER_ID, uid).apply();
        sp.edit().putString(StrHelper.SP_USER_EMAIL, email).apply();
        sp.edit().putString(StrHelper.SP_USER_TOKEN, token).apply();
    }

    private void showFirebaseError(FirebaseError firebaseError) {
        android.util.Log.e("errors", firebaseError.getMessage());
        switch (firebaseError.getCode()) {
            case FirebaseError.USER_DOES_NOT_EXIST:
                android.widget.Toast.makeText(context,"E-mail or password is invalid", Toast.LENGTH_SHORT).show();
                break;
            case FirebaseError.INVALID_EMAIL:
                android.widget.Toast.makeText(context,"E-mail or password is invalid", Toast.LENGTH_SHORT).show();
                break;
            default:
                android.widget.Toast.makeText(context,"Error logging in", Toast.LENGTH_SHORT).show();
                break;
        }
    }
    /* END LOGIN */

    /* LOG OUT */
    private void moveToLauncher() {
        Intent in = new Intent(context, LoginActivity.class);
        in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(in);
        ((Activity)context).finish();
    }

    public void setLogoutData() {
        sp.edit().putBoolean(StrHelper.SP_CHECK_LOGIN, false).apply();
        sp.edit().remove(StrHelper.SP_USER_EMAIL).apply();
        sp.edit().remove(StrHelper.SP_USER_TOKEN).apply();
        sp.edit().remove(StrHelper.SP_USER_ID).apply();
        if(sp.getBoolean(StrHelper.SP_FB_CHECK, false)) {
            LoginManager.getInstance().logOut();
            sp.edit().remove(StrHelper.SP_FB_ID).apply();
        }
        else
            ref.unauth();
        moveToLauncher();
    }
    /* END LOG OUT */

    /* REGISTER */
    public void register(final String email, final String password, final String username, final ProgressDialog progressDialog, final Button btnRegister) {
        ref.createUser(email, password, new Firebase.ValueResultHandler<Map<String, Object>>() {
            @Override
            public void onSuccess(Map<String, Object> stringObjectMap) {
                Toast.makeText(context, "You have been registered!", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                progressDialog.setMessage("Logging you in..");
                progressDialog.show();

                ref.authWithPassword(email, password, new Firebase.AuthResultHandler() {
                    @Override
                    public void onAuthenticated(AuthData authData) {
                        User u1 = new User(username, email, password, null);
                        progressDialog.dismiss();
                        try {
                            ref.child("user").child(authData.getUid()).setValue(u1);
                            setLoginData(false, authData.getUid(), email, authData.getToken());
                            moveActivity();
                        }
                        catch (Exception ex){
                            Toast.makeText(context, "Error logging in", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onAuthenticationError(FirebaseError firebaseError) {
                        progressDialog.dismiss();
                        btnRegister.setEnabled(true);
                        showFirebaseError(firebaseError);
                    }
                });
            }

            ;

            @Override
            public void onError(FirebaseError firebaseError) {
                showFirebaseError(firebaseError);
            }
        });
    }
    /* END REGISTER */

    /* GET SHARED PREFERENCED DATA */
    public String getEmail() {
        return sp.getString(StrHelper.SP_USER_EMAIL, null);
    }
    public String getUserFacebookId() {
        return sp.getString(StrHelper.SP_FB_ID, null);
    }
    public String getUId() {
        return sp.getString(StrHelper.SP_USER_ID, null);
    }
    /* END GET SHARED PREFERENCED DATA */

    /* OVERVIEW */
    public void getCurrentMonthExpense(final Date date) {
        ref.child("transaction").child(getUId()).addValueEventListener(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Calendar cal = Calendar.getInstance();
                        int month = cal.get(Calendar.MONTH);
                        int year = cal.get(Calendar.YEAR);
                        float expense = 0;
                        float income = 0;
                        for (DataSnapshot child : dataSnapshot.getChildren()) {
                            Transaction tr = child.getValue(Transaction.class);
                            cal.setTime(tr.getDate());
                            if(cal.get(Calendar.MONTH) == month && cal.get(Calendar.YEAR) == year) {
                                if(tr.getMoney() < 0) {
                                    expense += tr.getMoney();
                                }
                                else {
                                    income += tr.getMoney();
                                }
                            }
                        }
                        Log.v("yes", expense + " " + income);
                    }

                    @Override
                    public void onCancelled(FirebaseError firebaseError) {

                    }
                }
        );

    }
    /* END OVERVIEW */

    /* INPUT */
    public boolean inputTransaction(float money, String kategori, Date tanggal, String desc) {
        try {
            Transaction tr = new Transaction(getUId(), money, kategori, tanggal, desc);
            ref.child("transaction").child(getUId()).push().setValue(tr);
            return true;
        }
        catch (Exception ex) {
            Log.e("errors", ex.getMessage());
            Toast.makeText(context, "Fail to input transaction", Toast.LENGTH_SHORT).show();
        }
        return false;
    }
    /* END INPUT */

    /* REPORT */
    ArrayList<Entry> income, expense;
    ArrayList<String> labels;

    private void initArrayListReport() {
        income = new ArrayList<Entry>();
        expense = new ArrayList<Entry>();
        labels = new ArrayList<String>();
    }
    public void getTransactionPerMonth(final LineChart chart, final int year) {
        initArrayListReport();

        Query query = ref.child("transaction").child(sp.getString(StrHelper.SP_USER_ID, ""))
                .orderByChild("date");
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                int index = 0;
                float expenseMoney[] = new float[12];
                float incomeMoney[] = new float[12];
                Calendar cal = Calendar.getInstance();
                SimpleDateFormat sdf = new SimpleDateFormat("MMMM yyyy");

                for (int i = 0; i < 12; i++) {
                    expenseMoney[i] = incomeMoney[i] = 0;
                }
                for (DataSnapshot child : dataSnapshot.getChildren()) {
                    Transaction tr = child.getValue(Transaction.class);
                    cal.setTime(tr.getDate());
                    if(cal.get(Calendar.YEAR) == year) {
                        index = cal.get(Calendar.MONTH);
                        if(tr.getMoney() < 0)
                            expenseMoney[index] += Math.abs(tr.getMoney());
                        else
                            incomeMoney[index] += tr.getMoney();
                    }
                }
                for(int i = 0; i < 12; i++) {
                    cal.set(Calendar.MONTH, i);
                    cal.set(Calendar.YEAR, year);
                    income.add(new Entry(incomeMoney[i], i));
                    expense.add(new Entry(expenseMoney[i], i));
                    labels.add(sdf.format(cal.getTime()));
                }
                chart.setDescription("Monthly report");
                setDataToChart(chart);
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
    }
    public void getExpensePerMonth(final LineChart chart, final int year) {
        initArrayListReport();

        Query query = ref.child("transaction").child(sp.getString(StrHelper.SP_USER_ID, ""))
                .orderByChild("date");
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                int index = 0;
                float expenseMoney[] = new float[12];
                Calendar cal = Calendar.getInstance();
                SimpleDateFormat sdf = new SimpleDateFormat("MMMM yyyy");

                for (int i = 0; i < 12; i++) {
                    expenseMoney[i] = 0;
                }
                for (DataSnapshot child : dataSnapshot.getChildren()) {
                    Transaction tr = child.getValue(Transaction.class);
                    cal.setTime(tr.getDate());
                    if(cal.get(Calendar.YEAR) == year && tr.getMoney() < 0) {
                        index = cal.get(Calendar.MONTH);
                        expenseMoney[index] += Math.abs(tr.getMoney());
                    }
                }
                for(int i = 0; i < 12; i++) {
                    cal.set(Calendar.MONTH, i);
                    cal.set(Calendar.YEAR, year);
                    expense.add(new Entry(expenseMoney[i], i));
                    labels.add(sdf.format(cal.getTime()));
                }
                chart.setDescription("Monthly report");
                setDataToChart(chart);
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
    }
    public void getIncomePerMonth(final LineChart chart, final int year) {
        initArrayListReport();

        Query query = ref.child("transaction").child(sp.getString(StrHelper.SP_USER_ID, ""))
                .orderByChild("date");
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                int index = 0;
                float incomeMoney[] = new float[12];
                Calendar cal = Calendar.getInstance();
                SimpleDateFormat sdf = new SimpleDateFormat("MMMM yyyy");

                for (int i = 0; i < 12; i++) {
                    incomeMoney[i] = 0;
                }
                for (DataSnapshot child : dataSnapshot.getChildren()) {
                    Transaction tr = child.getValue(Transaction.class);
                    cal.setTime(tr.getDate());
                    if(cal.get(Calendar.YEAR) == year && tr.getMoney() > 0) {
                        index = cal.get(Calendar.MONTH);
                        incomeMoney[index] += tr.getMoney();
                    }
                }
                for(int i = 0; i < 12; i++) {
                    cal.set(Calendar.MONTH, i);
                    cal.set(Calendar.YEAR, year);
                    income.add(new Entry(incomeMoney[i], i));
                    labels.add(sdf.format(cal.getTime()));
                }
                chart.setDescription("Monthly report");
                setDataToChart(chart);
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
    }

    public void getTransactionPerYear(final LineChart chart) {
        initArrayListReport();

        Query query = ref.child("transaction").child(sp.getString(StrHelper.SP_USER_ID, ""))
                .orderByChild("date");
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                int index = 0;
                Calendar cal = Calendar.getInstance();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
                ArrayList<Integer> listYear = new ArrayList<Integer>();
                ArrayList<Float> expenseMoney = new ArrayList<Float>();
                ArrayList<Float> incomeMoney = new ArrayList<Float>();

                for (DataSnapshot child : dataSnapshot.getChildren()) {
                    Transaction tr = child.getValue(Transaction.class);
                    cal.setTime(tr.getDate());//masalah
                    if(!listYear.contains(cal.get(Calendar.YEAR))) {
                        listYear.add(cal.get(Calendar.YEAR));
                        labels.add(sdf.format(cal.getTime()));
                        if(tr.getMoney() < 0) {
                            expenseMoney.add(Math.abs(tr.getMoney()));
                        }
                        else {
                            incomeMoney.add(tr.getMoney());
                        }
                    }
                    else {
                        index = listYear.indexOf(cal.get(Calendar.YEAR));
                        if(tr.getMoney() < 0) {
                            if(expenseMoney.isEmpty())
                                expenseMoney.add(Math.abs(tr.getMoney()));
                            else
                                expenseMoney.set(index, expenseMoney.get(index) + Math.abs(tr.getMoney()));
                        }
                        else {
                            if(incomeMoney.isEmpty())
                                incomeMoney.add(tr.getMoney());
                            else
                                incomeMoney.set(index, expenseMoney.get(index) + tr.getMoney());
                        }

                    }
                }

                for(int i = 0; i < incomeMoney.size(); i++) {
                    income.add(new Entry(incomeMoney.get(i), i));
                }
                for(int i = 0; i < expenseMoney.size(); i++) {
                    expense.add(new Entry(expenseMoney.get(i), i));
                }

                chart.setDescription("Yearly report");
                setDataToChart(chart);
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
    }
    public void getExpensePerYear(final LineChart chart) {
        initArrayListReport();

        Query query = ref.child("transaction").child(sp.getString(StrHelper.SP_USER_ID, ""))
                .orderByChild("date");
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                int index = 0;
                Calendar cal = Calendar.getInstance();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
                ArrayList<Integer> listYear = new ArrayList<Integer>();
                ArrayList<Float> expenseMoney = new ArrayList<Float>();

                for (DataSnapshot child : dataSnapshot.getChildren()) {
                    Transaction tr = child.getValue(Transaction.class);
                    cal.setTime(tr.getDate());
                    if(!listYear.contains(cal.get(Calendar.YEAR))) {
                        listYear.add(cal.get(Calendar.YEAR));
                        labels.add(sdf.format(cal.getTime()));
                        if(tr.getMoney() < 0) {
                            expenseMoney.add(Math.abs(tr.getMoney()));
                        }
                    }
                    else {
                        index = listYear.indexOf(cal.get(Calendar.YEAR));
                        if(tr.getMoney() < 0) {
                            if(expenseMoney.isEmpty())
                                expenseMoney.add(Math.abs(tr.getMoney()));
                            else
                                expenseMoney.set(index, expenseMoney.get(index) + Math.abs(tr.getMoney()));
                        }
                    }
                }

                for(int i = 0; i < expenseMoney.size(); i++) {
                    expense.add(new Entry(expenseMoney.get(i), i));
                }

                chart.setDescription("Yearly report");
                setDataToChart(chart);
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
    }
    public void getIncomePerYear(final LineChart chart) {
        initArrayListReport();

        Query query = ref.child("transaction").child(sp.getString(StrHelper.SP_USER_ID, ""))
                .orderByChild("date");
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                int index = 0;
                Calendar cal = Calendar.getInstance();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
                ArrayList<Integer> listYear = new ArrayList<Integer>();
                ArrayList<Float> incomeMoney = new ArrayList<Float>();

                for (DataSnapshot child : dataSnapshot.getChildren()) {
                    Transaction tr = child.getValue(Transaction.class);
                    cal.setTime(tr.getDate());
                    if(!listYear.contains(cal.get(Calendar.YEAR))) {
                        listYear.add(cal.get(Calendar.YEAR));
                        labels.add(sdf.format(cal.getTime()));
                        if(tr.getMoney() > 0) {
                            incomeMoney.add(tr.getMoney());
                        }
                    }
                    else {
                        index = listYear.indexOf(cal.get(Calendar.YEAR));
                        if(tr.getMoney() > 0) {
                            if(incomeMoney.isEmpty())
                                incomeMoney.add(tr.getMoney());
                            else
                                incomeMoney.set(index, incomeMoney.get(index) + tr.getMoney());
                        }

                    }
                }

                for(int i = 0; i < incomeMoney.size(); i++) {
                    income.add(new Entry(incomeMoney.get(i), i));
                }

                chart.setDescription("Yearly report");
                setDataToChart(chart);
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
    }

    private void setDataToChart(LineChart chart) {
        ArrayList<LineDataSet> listSets = new ArrayList<LineDataSet>();
        LineDataSet incomeDataSet = new LineDataSet(income, "income in rupiah");
        LineDataSet expenseDataSet = new LineDataSet(expense, "expense in rupiah");
        incomeDataSet.setLineWidth(4f);
        incomeDataSet.setColor(Color.BLACK);
        expenseDataSet.setLineWidth(4f);
        expenseDataSet.setColor(Color.RED);

        listSets.add(incomeDataSet);
        listSets.add(expenseDataSet);

        LineData data = new LineData(labels, listSets);

        chart.animateX(5000);
        chart.animateY(5000);
        chart.setData(data);
        chart.invalidate();
    }


    /* END REPORT */

    /* BUDGET */

    /* END BUDGET */
}
