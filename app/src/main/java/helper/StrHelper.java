package helper;

/**
 * Created by Kevin Nathanael on 12/30/2015.
 */
public class StrHelper {

    public final static String SP_CHECK_LOGIN = "hasLoggedIn";
    public final static String SP_FB_CHECK = "viaFb";
    public final static String SP_FB_ID = "fbId";
    public final static String SP_USER = "user";
    public final static String SP_USER_ID = "uid";
    public final static String SP_USER_EMAIL = "email";
    public final static String SP_USER_IMG = "image";
    public final static String SP_USER_TOKEN = "token";

    public final static String URL_FR_DB = "https://money-assistant.firebaseio.com/data/";
}
