package helper;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Kevin Nathanael on 12/30/2015.
 */
public class LoadImage extends AsyncTask<Void, Void, Bitmap> {
    private String url;
    private CircleImageView imageView;

    public LoadImage(String url, CircleImageView imgView) {
        this.url = url;
        this.imageView = imgView;
    }

    @Override
    protected Bitmap doInBackground(Void... params) {
        try {
            URL urlConn = new URL(url);
            HttpURLConnection conn = (HttpURLConnection)urlConn.openConnection();
            conn.setDoInput(true);
            conn.connect();
            InputStream input = conn.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        super.onPostExecute(bitmap);
        imageView.setImageBitmap(bitmap);
    }
}
