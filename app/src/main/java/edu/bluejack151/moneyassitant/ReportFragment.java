package edu.bluejack151.moneyassitant;
import android.app.AlertDialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


public class ReportFragment extends Fragment {

//    public static TabLayout tabLayout;
//    public static ViewPager viewPager;
    public static int int_items = 3 ;
    int type;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        View x =  inflater.inflate(R.layout.fragment_report, null);


        SharedPreferences prefs = getActivity().getPreferences(Context.MODE_PRIVATE);
        int val = prefs.getInt("value",0);

        getActivity().setTitle("Report");

//        DialogFragment dialog = new DialogFragment(){
//            @Override
//            public Dialog onCreateDialog(Bundle savedInstanceState) {
//                //int title = getArguments().getInt("title");
//
//                return new AlertDialog.Builder(getActivity())
//                        .setTitle("Select Option")
//                        .setItems(R.array.option, new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                type = which;
//                            }
//                        }).create();
//            }
//        };
//        dialog.show(getFragmentManager(),"dialog");

        switch (val){
            case 0: //year
                FragmentTransaction fragTran = getFragmentManager().beginTransaction();
                fragTran.replace(R.id.isiFrame, new ReportByYearFragment());
                fragTran.commit();
            case 1: //month
                FragmentTransaction fragTran1 = getFragmentManager().beginTransaction();
                fragTran1.replace(R.id.isiFrame, new ReportByMonthFragment());
                fragTran1.commit();
            case 2: //category
                FragmentTransaction fragTran2 = getFragmentManager().beginTransaction();
                fragTran2.replace(R.id.isiFrame, new ReportByCategoryFragment());
                fragTran2.commit();
            default: //year
                FragmentTransaction fragTran3 = getFragmentManager().beginTransaction();
                fragTran3.replace(R.id.isiFrame, new ReportByYearFragment());
                fragTran3.commit();
        }


//        tabLayout = (TabLayout) x.findViewById(R.id.tabs);
//        viewPager = (ViewPager) x.findViewById(R.id.viewpager);
//        viewPager.setAdapter(new MyAdapter(getChildFragmentManager()));
//
//        tabLayout.post(new Runnable() {
//            @Override
//            public void run() {
//                tabLayout.setupWithViewPager(viewPager);
//            }
//        });

        return x;

    }

    class MyAdapter extends FragmentPagerAdapter{

        public MyAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position)
        {
            switch (position){
                case 0 : return new ExpenseReportByYearFragment();
                case 1 : return new BothReportByYearFragment();
                case 2 : return new IncomeReportByYearFragment();
            }
            return null;
        }

        @Override
        public int getCount() {

            return int_items;

        }

        @Override
        public CharSequence getPageTitle(int position) {

            switch (position){
                case 0 :
                    return "Expense";
                case 1 :
                    return "Both";
                case 2 :
                    return "Income";
            }
            return null;
        }
    }

}