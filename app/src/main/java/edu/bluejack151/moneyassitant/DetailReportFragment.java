package edu.bluejack151.moneyassitant;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import android.support.v7.widget.LinearLayoutCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;



public class DetailReportFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detail_report, container, false);

        LinearLayout detailLayout = (LinearLayout) view.findViewById(R.id.linearDetail);
        LinearLayoutCompat.LayoutParams lp = new LinearLayoutCompat.LayoutParams( LinearLayoutCompat.LayoutParams.WRAP_CONTENT,   LinearLayoutCompat.LayoutParams.WRAP_CONTENT);

        return view;
    }
}