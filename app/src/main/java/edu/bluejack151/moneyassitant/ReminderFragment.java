package edu.bluejack151.moneyassitant;

import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.os.SystemClock;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import helper.Bantuan;

public class ReminderFragment extends Fragment implements View.OnClickListener{
    //komponen
    private EditText amount, description, tanggal;
    private DatePickerDialog dateReminder;
    private Button btn_reminder;
    private TimePickerDialog timeReminder;
    private SimpleDateFormat dateFormatter;
    private String date, time;
    private int y, month, day, hour, min;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =inflater.inflate(R.layout.fragment_reminder, container, false);
        getActivity().setTitle("Reminder");
        amount = (EditText)view.findViewById(R.id.input_amount);
        description = (EditText)view.findViewById(R.id.input_description);
        //timeReminder = (TimePicker)view.findViewById(R.id.timeReminder);
        //dateReminder = (DatePicker)view.findViewById(R.id.dateReminder);
        btn_reminder = (Button)view.findViewById(R.id.btn_reminder);
        tanggal = (EditText) view.findViewById(R.id.edittanggal);
        tanggal.setInputType(InputType.TYPE_NULL);
        dateFormatter = new SimpleDateFormat("dd-MM-yyyy");

        tanggal.setOnClickListener(this);
        btn_reminder.setOnClickListener(this);

        Calendar newCalendar = Calendar.getInstance();
        dateReminder= new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                y = newDate.get(Calendar.YEAR);
                month = newDate.get(Calendar.MONTH);
                day = newDate.get(Calendar.DAY_OF_MONTH);
                date = dateFormatter.format(newDate.getTime());
                tanggal.setText(date + " " +time);


            }
        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        timeReminder = new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener(){

            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                time = hourOfDay +":"+minute;
                hour = hourOfDay;
                min = minute;
                tanggal.setText(date + " " +time);
            }
        },newCalendar.get(Calendar.HOUR), newCalendar.get(Calendar.MINUTE), false);




        return view;
    }
    private void scheduleNotification(Notification notification, int delay) {
        Intent notificationIntent = new Intent(getContext(), NotificationPublisher.class);
        notificationIntent.setData(Uri.parse("timer:" + Bantuan.INDEX_NOTIF));
        notificationIntent.putExtra(NotificationPublisher.notif, notification);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getContext(), 1, notificationIntent, 0);
        long futureInMillis = SystemClock.elapsedRealtime() + delay;

        AlarmManager alarmManager = (AlarmManager)getContext().getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, futureInMillis, pendingIntent);
    }
    private Notification getNotification(String notifDetail) {
        Notification.Builder builder = new Notification.Builder(getContext());
        builder.setContentTitle("Pay Your Debt");
        builder.setContentText(notifDetail);
        builder.setSmallIcon(R.drawable.bell_icon);
        return builder.build();
    }

    @Override
    public void onClick(View v) {
        if(v==btn_reminder) {
            String descText = description.getText().toString().trim();
            String amountText = amount.getText().toString().trim();
            if (amountText.equals("") || descText.equals("")) {
                Toast.makeText(getActivity(), "All fields must be filled", Toast.LENGTH_SHORT).show();
                return;
            }

//
//            int date = dateReminder.getDayOfMonth();
//            int month = dateReminder.getMonth();
//            int year = dateReminder.getYear();
//            timeReminder.clearFocus();
//            int hour = timeReminder.getCurrentHour();
//            int minute = timeReminder.getCurrentMinute();

            Calendar cal = Calendar.getInstance();
            long now = cal.getTimeInMillis();

            cal.set(y, month, day, hour, min, 0);
            long future = cal.getTimeInMillis();

            if (future - now <= 0) {
                Toast.makeText(getActivity(), "Invalid time", Toast.LENGTH_SHORT).show();
                return;
            }

            android.util.Log.v("yes", "f:" + future + " n:" + now + " " + (future - now));
            scheduleNotification(getNotification("Pay Rp" + amountText + " " + descText), (int) (future - now));
            Bantuan.INDEX_NOTIF++;
            Toast.makeText(getActivity(), "Reminder added successfully", Toast.LENGTH_SHORT).show();
            description.setText("");
            amount.setText("");
        }
        else if(v == tanggal){
            timeReminder.show();
            dateReminder.show();


        }
    }
}
