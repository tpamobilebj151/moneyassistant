package edu.bluejack151.moneyassitant;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by Kevin Nathanael on 1/3/2016.
 */
public class NotificationPublisher extends BroadcastReceiver {
    public static String notif = "notification";
    public static String notifId = "notification-id";

    @Override
    public void onReceive(Context context, Intent intent) {
        NotificationManager notificationManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
        Notification notification = intent.getParcelableExtra(notif);
        int id = Integer.parseInt(intent.getData().getSchemeSpecificPart());
        android.util.Log.v("yes", "KRING!");
        notificationManager.notify(id, notification);
    }
}
