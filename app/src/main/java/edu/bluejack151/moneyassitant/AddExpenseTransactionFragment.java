package edu.bluejack151.moneyassitant;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.annotation.Nullable;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;


import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import helper.Connection;
import helper.StrHelper;


public class AddExpenseTransactionFragment extends Fragment implements View.OnClickListener{
    Button btn_addExpense;
    Intent intent;
    private Connection conn;
    EditText money, desc, tanggal;
    Spinner spinner;
    DatePickerDialog trDatePicker;
    SimpleDateFormat dateFormatter;
    Date date;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_add_expense_transaction, container, false);

        dateFormatter = new SimpleDateFormat("dd-MM-yyyy");

        tanggal = (EditText) view.findViewById(R.id.edittanggal);
        spinner = (Spinner) view.findViewById(R.id.category);
        money = (EditText)view.findViewById(R.id.input);
        desc = (EditText)view.findViewById(R.id.description);
        tanggal.setInputType(InputType.TYPE_NULL);


        conn = new Connection(getContext(), StrHelper.URL_FR_DB);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(view.getContext(), R.array.category,
                android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        btn_addExpense = (Button)view.findViewById(R.id.btn_addE);
        btn_addExpense.setOnClickListener(this);
        tanggal.setOnClickListener(this);

        Calendar newCalendar = Calendar.getInstance();
        trDatePicker  = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                tanggal.setText(dateFormatter.format(newDate.getTime()));
                date = newDate.getTime();
            }
        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        return view;

    }

    @Override
    public void onClick(View v) {
        if(v == tanggal){
            trDatePicker.show();
        }
        else if(v == btn_addExpense){
            if(money.getText().toString().trim().equals("")) {
                    Toast.makeText(getContext(), "Amount must be filled", Toast.LENGTH_SHORT).show();
                    return;
                }

                float expense = Math.abs(Float.parseFloat(money.getText().toString().trim())) * -1;
                String description = desc.getText().toString().trim();


                if(conn.inputTransaction(expense, spinner.getSelectedItem().toString(), date ,description)) {
                    Toast.makeText(getContext(), "Transaction added successfully", Toast.LENGTH_SHORT).show();
                    intent = new Intent(getContext(), MainActivity.class);
                    startActivity(intent);
                }
        }
    }
}
