package edu.bluejack151.moneyassitant;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;


public class BudgetFragment extends Fragment {

    ProgressBar sisabudget;
    EditText addBudgetText;
    TextView of, budgetTotal;
    Handler handler = new Handler();
    int budgetskg, budgetmax, status=0;
    Button addBudgetbtn;


    @Nullable
    @Override

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View x =  inflater.inflate(R.layout.fragment_budget,null);

        addBudgetText = (EditText)x.findViewById(R.id.textAddBudget);
        addBudgetbtn = (Button)x.findViewById(R.id.btn_addBudget);
        sisabudget = (ProgressBar)x.findViewById(R.id.budgetbar);
        budgetTotal = (TextView)x.findViewById(R.id.textBudgettotal);

        budgetskg = 80; budgetmax = 120;

        sisabudget.setMax(budgetmax);
        of = (TextView)x.findViewById(R.id.of);

        budgetTotal.setText("Rp. " + budgetmax);

        new Thread(new Runnable() {
            @Override
            public void run() {
                while(status < budgetskg){
                    status += 1;

                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            sisabudget.setProgress(status);
                            of.setText(status + " of " + budgetmax);
                        }
                    });

                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();





        return x;

    }



}