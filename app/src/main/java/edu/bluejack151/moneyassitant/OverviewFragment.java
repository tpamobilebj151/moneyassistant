package edu.bluejack151.moneyassitant;


import android.content.Intent;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Calendar;

import helper.Connection;
import helper.StrHelper;

public class OverviewFragment extends Fragment{

        private Connection conn;

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {

            View view = inflater.inflate(R.layout.fragment_overview, container, false);
            getActivity().setTitle("Overview");

            FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab);
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getContext(), AddTransactionActivity.class);
                    startActivity(intent);
                }
            });

            conn = new Connection(getActivity(), StrHelper.URL_FR_DB);
            Calendar cal = Calendar.getInstance();
            conn.getCurrentMonthExpense(cal.getTime());

            return view;
        }
}
