package edu.bluejack151.moneyassitant;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentTransaction;

import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import de.hdodenhof.circleimageview.CircleImageView;
import helper.Connection;
import helper.LoadImage;
import helper.StrHelper;

public class MainActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private NavigationView navigationView;
    private DrawerLayout drawerLayout;
    private static final int TIME_DELAY = 3000;
    private static long back_pressed;

    private Connection conn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        conn = new Connection(this, StrHelper.URL_FR_DB);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FragmentTransaction fragTran = getSupportFragmentManager().beginTransaction();
        fragTran.replace(R.id.frame, new OverviewFragment());
        fragTran.commit();

        navigationView = (NavigationView) findViewById(R.id.navigation_view);

        // knomponen di nav header header
        View header = navigationView.inflateHeaderView(R.layout.header);
        TextView myname = (TextView)header.findViewById(R.id.username);
        TextView myemail= (TextView)header.findViewById(R.id.email);
        CircleImageView myimage = (CircleImageView) header.findViewById(R.id.profile_image);

        myemail.setText(conn.getEmail());
        myemail.invalidate();

        String userFbId = conn.getUserFacebookId();
        if(userFbId != null) {
            new LoadImage("https://graph.facebook.com/" + userFbId + "/picture?type=large", myimage).execute();
        }

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {


                if(menuItem.isChecked()) menuItem.setChecked(false);
                else menuItem.setChecked(true);

                drawerLayout.closeDrawers();

                switch (menuItem.getItemId()){


                    case R.id.overview:
                        //Toast.makeText(getApplicationContext(),"overview Selected",Toast.LENGTH_SHORT).show();

                        FragmentTransaction fragTran = getSupportFragmentManager().beginTransaction();
                        fragTran.replace(R.id.frame, new OverviewFragment());
                        fragTran.commit();

                        return true;


                    case R.id.budget:
                        FragmentTransaction fragTr = getSupportFragmentManager().beginTransaction();
                        fragTr.replace(R.id.frame, new BudgetFragment());
                        fragTr.commit();

                    case R.id.report:

                        //Toast.makeText(getApplicationContext(),"report Selected",Toast.LENGTH_SHORT).show();
                        DialogFragment dialog = new DialogFragment(){
                            @Override
                            public Dialog onCreateDialog(Bundle savedInstanceState) {
                                //int title = getArguments().getInt("title");

                                return new AlertDialog.Builder(getActivity())
                                        .setTitle("Select Option")
                                        .setItems(R.array.option, new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                SharedPreferences.Editor editor = getPreferences(MODE_PRIVATE).edit();
                                                editor.putInt("value",which);
                                                editor.apply();
                                            }
                                        }).create();
                            }
                        };
                        dialog.show(getSupportFragmentManager(),"dialog");

                        FragmentTransaction fragTrans = getSupportFragmentManager().beginTransaction();
                        fragTrans.replace(R.id.frame, new ReportFragment());
                        fragTrans.commit();

                        return true;

                    case R.id.reminder:
                        FragmentTransaction fragTranss = getSupportFragmentManager().beginTransaction();
                        fragTranss.replace(R.id.frame, new ReminderFragment());
                        fragTranss.commit();
                        return true;

                    case R.id.logout:
                        conn.setLogoutData();

                        return true;

                    default:
                        Toast.makeText(getApplicationContext(),"Somethings Wrong",Toast.LENGTH_SHORT).show();
                        return true;

                }
            }
        });

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer);
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this,drawerLayout,toolbar,R.string.navigation_drawer_open, R.string.navigation_drawer_close){

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };

        drawerLayout.setDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (back_pressed + TIME_DELAY > System.currentTimeMillis()) {
            super.onBackPressed();
        } else {
            Toast.makeText(getBaseContext(), "Press once again to exit!",
                    Toast.LENGTH_SHORT).show();
        }
        back_pressed = System.currentTimeMillis();

    }
}