package edu.bluejack151.moneyassitant;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONObject;

import java.util.Arrays;

import helper.Connection;
import helper.StrHelper;


public class LoginActivity extends AppCompatActivity {
    private static final int REQUEST_SIGNUP = 0;
    private static final int TIME_DELAY = 3000;
    private static long back_pressed;

    EditText emailText;
    EditText passwordText;
    Button loginButton;
    TextView signupLink;
    LoginButton loginfbButton;
    CallbackManager callbackManager;

    private Connection conn;
    static SharedPreferences sp;
    String emailku;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_login);

        conn = new Connection(this, "https://money-assistant.firebaseio.com/data/");
        sp = getApplicationContext().getSharedPreferences(StrHelper.SP_USER, Context.MODE_PRIVATE);

        emailText = (EditText)findViewById(R.id.input_email);
        passwordText = (EditText)findViewById(R.id.input_password);
        loginButton = (Button)findViewById(R.id.btn_login);
        signupLink = (TextView)findViewById(R.id.link_signup);
        loginfbButton = (LoginButton)findViewById(R.id.btn_fb);

        //cek udah login atau belum
        conn.checkAuth();

        //login facebook
        callbackManager = CallbackManager.Factory.create();
        loginfbButton.setReadPermissions(Arrays.asList("public_profile, email"));
        loginfbButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(final LoginResult loginResult) {
                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    final LoginResult loginResult1 = loginResult;
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        try {
                            LoginActivity.this.emailku = object.getString("email");
                            conn.loginViaFacebook(LoginActivity.this.emailku, loginResult1.getAccessToken().getToken(), loginResult1.getAccessToken().getUserId());
                        } catch (Exception ex) {
                            android.util.Log.e("error", ex.getMessage());
                        }
                    }
                });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id, email");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                Toast.makeText(getApplicationContext(), "Login cancelled", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException error) {
                Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_SHORT).show();
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                login();
            }
        });

        signupLink.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // intent signup
                Intent intent = new Intent(getApplicationContext(), SignUpActivity.class);
                startActivityForResult(intent, REQUEST_SIGNUP);
                finish();
            }
        });
    }

    public void login() {

        if (!validate()) {
            onLoginFailed();
            return;
        }

        loginButton.setEnabled(false);

        final ProgressDialog progressDialog = new ProgressDialog(LoginActivity.this,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Authenticating...");
        progressDialog.show();

        final String email = emailText.getText().toString().trim();
        final String password = passwordText.getText().toString().trim();

        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        conn.login(email, password, progressDialog, loginButton);
                        //onLoginSuccess();
                        // onLoginFailed();
                        //progressDialog.dismiss();
                    }
                }, TIME_DELAY);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_SIGNUP) {
            if (resultCode == RESULT_OK) {
                //masukin ke activity awal2
                this.finish();

            }
        }else{
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onBackPressed() {
        if (back_pressed + TIME_DELAY > System.currentTimeMillis()) {
            super.onBackPressed();
        } else {
            Toast.makeText(getBaseContext(), "Press once again to exit!",
                    Toast.LENGTH_SHORT).show();
        }
        back_pressed = System.currentTimeMillis();

    }

    public void onLoginSuccess() {
        loginButton.setEnabled(true);
        //masukin intent utk masuk ke halaman awal
        SharedPreferences sharedPref = getSharedPreferences("user", Context.MODE_PRIVATE);
        sharedPref.edit()
                .putString("email", emailText.getText().toString())
                        //.putString("name", name)
                        //.putString("phone", phone)
                .apply();

        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();

    }

    public void onLoginFailed() {
        Toast.makeText(getBaseContext(), "Login failed", Toast.LENGTH_LONG).show();
        loginButton.setEnabled(true);
    }

    public boolean validate() {
        boolean valid = true;

        String email = emailText.getText().toString();
        String password = passwordText.getText().toString();

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            emailText.setError("enter a valid email address");
            valid = false;
        } else {
            emailText.setError(null);
        }

        if (password.isEmpty()) {
            passwordText.setError("enter your password");
            valid = false;
        } else {
            passwordText.setError(null);
        }

        return valid;
    }
}
